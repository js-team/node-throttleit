# Installation
> `npm install --save @types/throttleit`

# Summary
This package contains type definitions for throttleit ( https://github.com/component/throttle ).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/throttleit

Additional Details
 * Last updated: Tue, 16 Apr 2019 19:36:17 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by Ifiok Jr. <https://github.com/ifiokjr>.
